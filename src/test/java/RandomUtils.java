import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import java.util.Random;

public class RandomUtils {
    private static final String ENGLISH_ALPHABET = "abcdefghijklmnopqrstuvwxyz";
    private static final String NUMBERS = "0123456789";

    private static String getRandomString(int length, String characters) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int randomIndex = random.nextInt(characters.length());
            sb.append(characters.charAt(randomIndex));
        }
        return sb.toString();
    }

    public static String getRandomNumbers(int length) {
        return getRandomString(length, NUMBERS);
    }

    public static String getRandomLetters(int length) {
        return getRandomString(length, ENGLISH_ALPHABET);
    }

    public static String getRandomString(int length) {
        return getRandomString(length, ENGLISH_ALPHABET.toUpperCase() + ENGLISH_ALPHABET + NUMBERS);
    }

    public static SelenideElement getRandomElement(ElementsCollection elementsCollection) {
        Random random = new Random();
        return elementsCollection.get(random.nextInt(elementsCollection.size()));
    }
}
