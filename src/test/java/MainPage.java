import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

/**
 * Main page of the site userinyerface.com
 */
public class MainPage {
    private final static String BASE_URL = "https://userinyerface.com/";
    private final SelenideElement link = $x("//a[@class='start__link']");

    public void clickOnLink() {
        link.click();
    }

    public void openWebPage() {
        Selenide.open(BASE_URL);
    }
}
