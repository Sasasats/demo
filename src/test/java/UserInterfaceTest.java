import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class UserInterfaceTest extends BaseTest{
    private final String TIMER_DEFAULT_VALUE = "00:00:00";

    @BeforeMethod
    private void beforeMethod() {
        MainPage mainPage = new MainPage();
        mainPage.openWebPage();
        mainPage.clickOnLink();
        GamePage gamePage = new GamePage();
        Assert.assertEquals(gamePage.getPageNumber(), "1", "Page 1 isn't opened");
    }

    @Test
    public void testCase1() {
        GamePage gamePage = new GamePage();
        gamePage.login();
        Assert.assertEquals(gamePage.getPageNumber(), "2", "Page 2 isn't opened");
    }

    @Test
    public void testCase2() {
        GamePage gamePage = new GamePage();
        gamePage.hideHelpForm();
        Assert.assertFalse(gamePage.isHelpFormDisplayed(), "Help form is displayed");
    }

    @Test
    public void testCase3() {
        GamePage gamePage = new GamePage();
        gamePage.acceptCookies();
        Assert.assertFalse(gamePage.isCookiesBannerDisplayed(), "Cookies banner is displayed");
    }

    @Test
    public void testCase4() {
        GamePage gamePage = new GamePage();
        Assert.assertEquals(gamePage.getTimerText(), TIMER_DEFAULT_VALUE, "Timer does not start from zero");
    }
}
