import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class GamePage {
    private final SelenideElement cookiesBanner = $x("//div[@class='cookies']");
    private final SelenideElement acceptCookiesButton = $x("//button[contains(@class,'button--transparent')]");
    private final SelenideElement timer = $x("//div[contains(@class,'timer')]");
    private final SelenideElement pageIndicator = $x("//div[@class='page-indicator']");
    private final SelenideElement passwordInput = $x("//input[@placeholder='Choose Password']");
    private final SelenideElement emailInput = $x("//input[@placeholder='Your email']");
    private final SelenideElement domainInput = $x("//input[@placeholder='Domain']");
    private final SelenideElement domainDropdownOpener = $x("//div[@class='dropdown__opener']");
    private final ElementsCollection domains = $$x("//div[@class='dropdown__list-item']");
    private final SelenideElement termsConditionsCheckbox = $x("//input[@id='accept-terms-conditions']/following-sibling::span");
    private final SelenideElement nextButton = $x("//a[@class='button--secondary']");
    private final SelenideElement helpForm = $x("//div[@class='help-form']");
    private final SelenideElement sendToBottomButton = $x("//button[contains(@class,'send-to-bottom-button')]");

    public boolean isCookiesBannerDisplayed() {
        return cookiesBanner.isDisplayed();
    }

    public void acceptCookies() {
        acceptCookiesButton.click();
    }

    public String getTimerText() {
        return timer.getText();
    }

    public String getPageNumber() {
        return pageIndicator.getText().substring(0, 1);
    }

    public void login() {
        String password = RandomUtils.getRandomLetters(1).toUpperCase() +
                RandomUtils.getRandomLetters(1) +
                RandomUtils.getRandomNumbers(1) +
                RandomUtils.getRandomString(7);
        String email = password.substring(5);
        String domain = password.substring(5);
        setPassword(password);
        setEmail(email);
        setDomain(domain);
        chooseRandomDomain();
        clickTermsConditionsCheckbox();
        clickNextButton();
    }

    private void setPassword(String password) {
        passwordInput.clear();
        passwordInput.sendKeys(password);
    }

    private void setEmail(String email) {
        emailInput.clear();
        emailInput.sendKeys(email);
    }

    private void setDomain(String domain) {
        domainInput.clear();
        domainInput.sendKeys(domain);
    }

    private void chooseRandomDomain() {
        domainDropdownOpener.click();
        RandomUtils.getRandomElement(domains).click();
    }

    private void clickTermsConditionsCheckbox() {
        termsConditionsCheckbox.click();
    }

    private void clickNextButton() {
        nextButton.click();
    }

    public boolean isHelpFormDisplayed() {
        return helpForm.isDisplayed();
    }

    public void hideHelpForm() {
        sendToBottomButton.click();
    }
}
